unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ScktComp, Grids;

const LENMSG=20;

type
    TCard = record
  val:byte;
  colr:byte;
  poker:byte;
  end;
  TForm1 = class(TForm)
    ServerSocket1: TServerSocket;
    Button1: TButton;
    Edit1: TEdit;
    Label1: TLabel;
    ListBox1: TListBox;
    Button2: TButton;
    Edit2: TEdit;
    Button3: TButton;
    Button4: TButton;
    sg1: TStringGrid;
    ng: TStringGrid;
    Button5: TButton;
    Button6: TButton;
    theatr: TListBox;
    Button7: TButton;
    Button8: TButton;
    Button9: TButton;
    Memo1: TMemo;
    theatr2: TMemo;
    procedure InitGrid(cntpl:byte);
    procedure CardRound(x:byte);
    procedure CardInit;
    procedure ChooseCard(x:byte);
    procedure Button1Click(Sender: TObject);
    procedure ServerSocket1ClientRead(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    Function max(x: array of TCard):byte;
    procedure Button6Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    Function parser(x:Tcard):string;
    procedure Button7Click(Sender: TObject);
    procedure Edit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  cards: record
        ar:array [0..35] of TCard;
        cnt:byte;
        end;
  card:TCard;
  rndcrds:array [0..9] of TCard;
  numround:byte;
  numplayer:byte;
  tmpplayer,winplayer,rndplayer:byte;
  cntpl:byte;
  points:array [0..9] of integer;
implementation

{$R *.dfm}

function TForm1.parser(x:Tcard):string;
var tmpstr:string;
begin
tmpstr:='';
case x.val of
0..3:tmpstr:=inttostr(x.val+6)+'     ';
4:tmpstr:='10    ';
5:tmpstr:='����� ';
6:tmpstr:='����  ';
7:tmpstr:='������';
8:tmpstr:='���   ';
end;
case x.colr of
0:tmpstr:=tmpstr+'������';
1:tmpstr:=tmpstr+'����  ';
2:tmpstr:=tmpstr+'����  ';
3:tmpstr:=tmpstr+'����� ';
end;
if x.poker>0 then tmpstr:='JOKER';
parser:=tmpstr;
end;

Function TForm1.max(x:array of TCard):byte;
var i,maxpl:byte;
    maxcard:TCard;
begin
maxpl:=rndplayer;
maxcard:=rndcrds[rndplayer];
rndplayer:=(rndplayer+1) mod cntpl;
for i:=0 to cntpl-1 do
  begin
   if (rndcrds[rndplayer].poker > 0)then
    begin
      maxcard:=rndcrds[rndplayer];
      maxpl:=rndplayer;
      max:=maxpl;
      exit;
    end;
   if (rndcrds[rndplayer].colr=maxcard.colr) then
    begin
      if (rndcrds[rndplayer].val>maxcard.val) then
          begin
          maxcard:=rndcrds[rndplayer];
          maxpl:=rndplayer;
          end;
    end
   else
      if (rndcrds[rndplayer].colr=3) and (maxcard.colr<>3) then
      begin
      maxcard:=rndcrds[rndplayer];
      maxpl:=rndplayer;
      end;
  rndplayer:=(rndplayer+1) mod cntpl;
  end;
rndplayer:=maxpl;
max:=maxpl;
end;

procedure TForm1.CardRound(x:byte);
var i,j,tmp:byte;
begin
CardInit;
for i:=0 to x-1 do
  for j:=0 to ServerSocket1.Socket.ActiveConnections-1 do
  begin
    tmp:=random(cards.cnt+1);
    ServerSocket1.Socket.Connections[j].SendText('@'+IntToStr(cards.ar[tmp].val)+IntToStr(cards.ar[tmp].colr)+'0');
    theatr2.Lines.Add('@'+IntToStr(cards.ar[tmp].val)+IntToStr(cards.ar[tmp].colr)+'0');
    ChooseCard(tmp);
    sleep(250);
  end;
inc(numround);
numplayer:=(numplayer+1) mod  ServerSocket1.Socket.ActiveConnections;
end;

procedure TForm1.ChooseCard(x: byte);
var i:integer;
begin
for i:=x to cards.cnt-1 do
  begin
  cards.ar[i]:=cards.ar[i+1];
  end;
  dec(cards.cnt);
end;

procedure TForm1.CardInit;
var i:integer;
begin
for  i:=0 to 35 do
  begin
    cards.ar[i].val:=i mod 9;
    cards.ar[i].colr:=i div 9;
  end;
  cards.cnt:=35;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
ServerSocket1.Port:=StrToInt(Edit1.Text);
ServerSocket1.Open;
numround:=0;cntpl:=0;
button1.Enabled:=false;
end;

procedure TForm1.ServerSocket1ClientRead(Sender: TObject;
  Socket: TCustomWinSocket);
var i,j,tmp:integer;
    stext,strtmp,tmpname:string;
begin
stext:=socket.ReceiveText;
case stext[1] of
  '%':
      begin
      setlength(strtmp,length(stext)-1);
      for i:=1 to length(stext)-1 do strtmp[i]:=stext[i+1];
      ng.Cells[0,cntpl]:=strtmp;
      for i:=0 to ServerSocket1.Socket.ActiveConnections-1 do
        begin
        ServerSocket1.Socket.Connections[i].SendText('SERVER:� ���� ����� '+strtmp);
        sleep(100);
        end;
      inc(cntpl);
      end;
  '@':
      begin
      setlength(strtmp,length(stext)-1);
          for i:=1 to length(stext)-1 do strtmp[i]:=stext[i+1];
          sleep(250);
          for j:=0 to ServerSocket1.Socket.ActiveConnections-1 do
            begin
            ServerSocket1.Socket.Connections[j].SendText('SERVER:��� ������� '+strtmp);
            sleep(100);
            end;
      end;
  '$'://
      if ServerSocket1.Socket.Connections[tmpplayer]=socket then
      begin
      stext[1]:='0';
      tmp:=StrToInt(stext);
      for i:=0 to cntpl-1 do
        begin
        ServerSocket1.Socket.Connections[i].SendText('$'+IntTostr(tmpplayer)+IntToStr(tmp));
        sleep(100);
        end;
      sg1.Cells[tmpplayer+1,numround-1]:=Inttostr(tmp);
      tmpplayer:=(tmpplayer+1) mod cntpl;
      if tmpplayer<>rndplayer then
      for i:=0 to cntpl-1 do
        begin
        ServerSocket1.Socket.Connections[i].SendText('>>SERVER:����� '+sg1.Cells[tmpplayer+1,0]);
        sleep(100);
        end
      else
        for i:=0 to cntpl-1 do
          begin
          ServerSocket1.Socket.Connections[i].SendText('>>SERVER:����� '+sg1.Cells[tmpplayer+1,0]);
          sleep(100);
          end;
      end;
  '#':
      if ServerSocket1.Socket.Connections[tmpplayer]=socket then
      begin
      card.val:=StrtoInt(stext[2]);
      card.colr:=StrtoInt(stext[3]);
      card.poker:=strtoint(stext[4]);
      rndcrds[tmpplayer]:=card;
      theatr2.Lines.Add(sg1.Cells[tmpplayer+1,0]+'>>'+parser(card));
      for i:=0 to cntpl-1 do
        begin
        sleep(100);
        ServerSocket1.Socket.Connections[i].SendText('#'+inttostr(card.val)+inttostr(card.colr)
                                                    +inttostr(card.poker)+inttostr(tmpplayer));
        sleep(100);
        end;
        tmpplayer:=(tmpplayer+1) mod cntpl;
      if tmpplayer=rndplayer then
          begin
          winplayer:=max(rndcrds);
          tmpplayer:=winplayer;
          sg1.Cells[winplayer+1,numround-1]:=sg1.Cells[winplayer+1,numround-1]+'|';
          sleep(2000);
          theatr2.Lines.Add('WINNER: '+sg1.Cells[winplayer+1,0]);
          //theatr.Selected[theatr.Items.Count-1]:=true;
          for i:=0 to cntpl-1 do
            begin
            ServerSocket1.Socket.Connections[i].SendText('?'+IntTostr(winplayer));
            sleep(100);
            end;
          sleep(500);
          for i:=0 to cntpl-1 do
            begin
            ServerSocket1.Socket.Connections[i].SendText('>>SERVER:����� '+sg1.cells[winplayer+1,0]);
            sleep(100);
            end;
          end
      else
      for i:=0 to cntpl-1 do
          begin
          ServerSocket1.Socket.Connections[i].SendText('>>SERVER:����� '+sg1.Cells[tmpplayer+1,0]);
          sleep(100);
          end;
      end;
  else
  begin
    for i:=0 to cntpl-1 do if ServerSocket1.Socket.Connections[i]=socket then stext:=sg1.Cells[i+1,0]+' >> '+stext;
    //listbox1.Items.Add(stext);
    for i:=0 to cntpl-1 do
      begin
      ServerSocket1.Socket.Connections[i].SendText(stext);
      sleep(100);
      end;
      {
    if (length(stext)<=LENMSG) then
    listbox1.Items.Add(stext)
    else
    begin
    tmp:=length(stext) div LENMSG;
    for i:=1 to tmp do
      begin
        tmpname:=copy(stext,(i-1)*LENMSG+1,LENMSG);
        if ((i*LENMSG)<length(stext))and(stext[i*LENMSG]<>' ')and(stext[i*LENMSG+1]<>' ') then
            tmpname:=tmpname+'-';
        listbox1.Items.Add(tmpname);
      end;
    i:=length(stext)-tmp*LENMSG;
    tmpname:='';
    for j:=1 to i do tmpname:=tmpname+stext[tmp*LENMSG+j];
    listbox1.Items.Add(tmpname);
    end;
    listbox1.Selected[listbox1.items.Count-1]:=true;
    }
  memo1.Lines.Add(stext);
  end;
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
Close;
end;

procedure TForm1.Button4Click(Sender: TObject);
var i,tmp,max:integer;
begin
rndplayer:=(numround-1) mod cntpl;
for i:=0 to cntpl-1 do
  begin
  ServerSocket1.Socket.Connections[i].SendText('>>SERVER:����� '+sg1.Cells[rndplayer+1,0]);
  sleep(100);
  end;
tmpplayer:=rndplayer;
CardRound(StrToInt(sg1.cells[0,numround]));
theatr2.Lines.Add('--------------------');
end;

procedure TForm1.InitGrid(cntpl:byte);
var i,j:byte;
begin
sg1.ColCount:=cntpl+1;
for i:=1 to 36 div cntpl do
  sg1.Cells[0,i]:=IntToStr(i);
j:=0;dec(i);
for j:=0 to cntpl-3 do sg1.Cells[0,i+j+1]:=IntToStr(i);
j:=j+i+1;
for i:= 36 div cntpl downto 1 do
  begin
    sg1.Cells[0,j]:=IntToStr(i);
    inc(j);
  end;
for i:=1 to cntpl-1 do
  begin
    sg1.Cells[0,j]:='1';
    inc(j);
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
var i:integer;
    n:string;
begin
randomize;
rndplayer:=0;
numplayer:=ServerSocket1.Socket.ActiveConnections-1;
tmpplayer:=0;
n:=''; cntpl:= ServerSocket1.Socket.ActiveConnections;
InitGrid(ServerSocket1.Socket.ActiveConnections);
numround:=1;
n:=IntToStr(ServerSocket1.Socket.ActiveConnections)+' ';
for i:=1 to ServerSocket1.Socket.ActiveConnections do
  begin
  sg1.Cells[i,0]:=ng.Cells[0,i-1];
  n:=n+ng.Cells[0,i-1]+' ';
  end;
for i:=0 to ServerSocket1.Socket.ActiveConnections-1 do
begin
ServerSocket1.Socket.Connections[i].SendText('%'+n);
sleep(100);
end;
for i:=0 to cntpl-1 do
  points[i]:=0;
end;

procedure TForm1.Button6Click(Sender: TObject);
var i,j,tmp:byte;
    st,tmpname:string;
begin
for i:=0 to cntpl-1 do
  begin
  ServerSocket1.Socket.Connections[i].SendText(edit2.Text);
  sleep(100);
  end;
  {
st:=edit2.Text;
if (length(st)<=LENMSG) then
    listbox1.Items.Add(st)
  else
    begin
    tmp:=length(st) div LENMSG;
    for i:=1 to tmp do
      begin
        tmpname:=copy(st,(i-1)*LENMSG+1,LENMSG);
        if ((i*LENMSG)<length(st))and(st[i*LENMSG]<>' ')and(st[i*LENMSG+1]<>' ') then
            tmpname:=tmpname+'-';
        listbox1.Items.Add(tmpname);
      end;
    i:=length(st)-tmp*LENMSG;
    tmpname:='';
    for j:=1 to i do tmpname:=tmpname+st[tmp*LENMSG+j];
    listbox1.Items.Add(tmpname);
    end;
listbox1.Selected[listbox1.Items.Count-1]:=true;
edit2.Text:=''; }
memo1.Lines.Add(edit2.Text);
edit2.Text:='';
end;

procedure TForm1.Button5Click(Sender: TObject);
var stext,ord:string;
    i,j,fact:byte;
begin
for i:=0 to cntpl-1 do
  begin
    stext:=sg1.Cells[i+1,numround-1];
    j:=1;ord:='00';
    while (j<=length(stext)) and (stext[j]<>'|') do
      begin
      ord[j]:=stext[j];
      inc(j);
      end;
    setlength(ord,j-1);
    fact:=length(stext)-j+1;
    if fact>StrToint(ord) then
        begin
          points[i]:=points[i]+fact;
        end
    else
        if fact<StrToint(ord) then
          begin
            points[i]:=points[i]-(StrToInt(ord)-fact)*10;
          end
        else if fact<>0 then points[i]:=points[i]+fact*10
              else points[i]:=points[i]+5;
    sg1.Cells[i+1,numround-1]:=stext+IntToStr(points[i]);
  end;
stext:='!';
for i:=0 to cntpl-1 do
    stext:=stext+inttostr(points[i])+' ';
for i:=0 to cntpl-1 do
  begin
    serversocket1.Socket.Connections[i].SendText(stext);
    sleep(100);
  end;
end;

procedure TForm1.Button7Click(Sender: TObject);
var i:integer;
begin
for i:=0 to cntpl-1 do
  begin
  sleep(100);
  ServerSocket1.Socket.Connections[i].SendText('*');
  end;
end;

procedure TForm1.Edit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var i,j,tmp:integer;
      st,tmpname:string;
begin
if key=13 then
  begin
  for i:=0 to cntpl-1 do
    begin
    ServerSocket1.Socket.Connections[i].SendText(edit2.Text);
    sleep(250);
    end;
  st:=edit2.Text;
  {
  if (length(st)<=LENMSG) then
      listbox1.Items.Add(st)
  else
    begin
      tmp:=length(st) div LENMSG;
      for i:=1 to tmp do
      begin
        tmpname:=copy(st,(i-1)*LENMSG+1,LENMSG);
        if ((i*LENMSG)<length(st))and(st[i*LENMSG]<>' ')and(st[i*LENMSG+1]<>' ') then
            tmpname:=tmpname+'-';
        listbox1.Items.Add(tmpname);
      end;
       i:=length(st)-tmp*LENMSG;
      tmpname:='';
      for j:=1 to i do tmpname:=tmpname+st[tmp*LENMSG+j];
      listbox1.Items.Add(tmpname);
    end;
  listbox1.Selected[listbox1.Items.Count-1]:=true;
  }
  memo1.Lines.Add(edit2.text);
  edit2.Text:='';
  end;
end;

procedure TForm1.Button8Click(Sender: TObject);
var i,j:integer;
begin
for i:=0 to sg1.ColCount-1 do
  for j:=0 to sg1.RowCount-1 do
    sg1.Cells[i,j]:='';
ServerSOcket1.Close;
button1.Enabled:=true;
end;

procedure TForm1.Button9Click(Sender: TObject);
begin
showmessage('����� ���� - ����������� �.�.'+#13+'ICQ - 485271451'+#13+'http://vkontakte.ru/fuckinoff');
end;

procedure TForm1.Label2Click(Sender: TObject);
begin
button1.Click;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
memo1.Clear;
theatr2.Clear;
end;

end.
