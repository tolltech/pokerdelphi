object Form1: TForm1
  Left = 220
  Top = 190
  Anchors = [akLeft, akBottom]
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'ServerPokerGame by FuckoFF'
  ClientHeight = 602
  ClientWidth = 854
  Color = clGreen
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 66
    Height = 13
    Caption = #1053#1086#1084#1077#1088' '#1087#1086#1088#1090#1072
  end
  object Button1: TButton
    Left = 8
    Top = 32
    Width = 145
    Height = 33
    Caption = #1047#1072#1087#1091#1089#1090#1080#1090#1100
    TabOrder = 0
    OnClick = Button1Click
  end
  object Edit1: TEdit
    Left = 88
    Top = 8
    Width = 65
    Height = 21
    Color = clMoneyGreen
    TabOrder = 1
    Text = '7777'
  end
  object ListBox1: TListBox
    Left = 168
    Top = 81
    Width = 17
    Height = 24
    Color = clLime
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = False
    TabOrder = 2
    Visible = False
  end
  object Button2: TButton
    Left = 0
    Top = 520
    Width = 89
    Height = 25
    Caption = #1042#1099#1093#1086#1076
    TabOrder = 3
    OnClick = Button2Click
  end
  object Edit2: TEdit
    Left = 8
    Top = 456
    Width = 177
    Height = 21
    Color = clLime
    TabOrder = 4
    Text = ' '
    OnKeyDown = Edit2KeyDown
  end
  object Button3: TButton
    Left = 8
    Top = 72
    Width = 137
    Height = 25
    Caption = #1053#1040#1063#1040#1058#1068' '#1048#1043#1056#1059
    TabOrder = 5
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 8
    Top = 104
    Width = 137
    Height = 25
    Caption = 
      '------------------------------------------------'#1056#1040#1059#1053#1044'-----------' +
      '-----------------------------------'
    TabOrder = 6
    OnClick = Button4Click
  end
  object sg1: TStringGrid
    Left = 192
    Top = 8
    Width = 497
    Height = 577
    Color = clCream
    ColCount = 2
    DefaultColWidth = 100
    DefaultRowHeight = 14
    Enabled = False
    RowCount = 38
    TabOrder = 7
    RowHeights = (
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14
      14)
  end
  object ng: TStringGrid
    Left = 712
    Top = 16
    Width = 123
    Height = 120
    ColCount = 1
    FixedCols = 0
    RowCount = 4
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 8
  end
  object Button5: TButton
    Left = 8
    Top = 136
    Width = 137
    Height = 25
    Caption = #1047#1040#1050#1054#1053#1063#1048#1058#1068' '#1056#1040#1059#1053#1044
    TabOrder = 9
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 8
    Top = 488
    Width = 129
    Height = 25
    Caption = #1054#1058#1055#1056#1040#1042#1048#1058#1068
    TabOrder = 10
    Visible = False
    OnClick = Button6Click
  end
  object theatr: TListBox
    Left = 704
    Top = 584
    Width = 137
    Height = 17
    Color = clMoneyGreen
    ItemHeight = 13
    TabOrder = 11
    Visible = False
  end
  object Button7: TButton
    Left = 704
    Top = 544
    Width = 137
    Height = 33
    Caption = #1047#1072#1082#1086#1085#1095#1080#1090#1100' '#1080#1075#1088#1091
    TabOrder = 12
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 96
    Top = 520
    Width = 89
    Height = 25
    Caption = #1057#1073#1088#1086#1089' '#1048#1075#1088#1099
    TabOrder = 13
    OnClick = Button8Click
  end
  object Button9: TButton
    Left = 0
    Top = 560
    Width = 185
    Height = 33
    Caption = #1057#1086#1079#1076#1072#1090#1077#1083#1080
    TabOrder = 14
    OnClick = Button9Click
  end
  object Memo1: TMemo
    Left = 8
    Top = 168
    Width = 177
    Height = 281
    Color = clLime
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssVertical
    TabOrder = 15
  end
  object theatr2: TMemo
    Left = 704
    Top = 144
    Width = 137
    Height = 385
    Color = clMoneyGreen
    Lines.Strings = (
      'theatr2')
    ScrollBars = ssVertical
    TabOrder = 16
  end
  object ServerSocket1: TServerSocket
    Active = False
    Port = 0
    ServerType = stNonBlocking
    OnClientRead = ServerSocket1ClientRead
    Left = 160
    Top = 8
  end
end
